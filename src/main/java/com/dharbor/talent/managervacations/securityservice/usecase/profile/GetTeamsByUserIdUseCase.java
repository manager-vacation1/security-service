package com.dharbor.talent.managervacations.securityservice.usecase.profile;

import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.ListUserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.mapper.ProfileStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetTeamsByUserIdUseCase {

    private IProfileService iProfileService;

    private ProfileStructMapper profileStructMapper;

    public ListUserAndProfileResponse execute(Long id) {
        Profile profile = iProfileService.findByUser_Id(id);
        List<Profile> profileList = iProfileService.findAllByTeam(profile.getTeam());
        List<ProfileDto> profileDtoList = profileStructMapper.profileListtoProfileDtio(profileList);
        return new ListUserAndProfileResponse(profileDtoList);
    }

}
