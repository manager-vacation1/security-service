package com.dharbor.talent.managervacations.securityservice.gateway.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class CountryDto {
    private Long id;
    private String code;
    private String name;
}
