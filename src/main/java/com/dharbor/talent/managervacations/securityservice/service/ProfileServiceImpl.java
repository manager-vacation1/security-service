package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.repository.ProfileRepository;
import com.dharbor.talent.managervacations.securityservice.validator.*;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
@Log4j2
public class ProfileServiceImpl implements IProfileService {

    private ProfileRepository profileRepository;

    private Verifications verifications;

    private Message message;


    @Override
    public Profile save(Profile profile) {
        validateProfile(profile);
        return profileRepository.save(profile);
    }

    @Override
    public Profile findByUser_Id(Long userId) {
        verifications.verify(new VerifyExistUserCommand(message.getMessage("Exist.profile.userId.message"),
                profileRepository.existsByUser_Id(userId)));
        return profileRepository.findByUser_Id(userId);
    }

    @Override
    public boolean existsByUser_Id(Long userId) {
        return profileRepository.existsByUser_Id(userId);
    }

    @Override
    public List<Profile> findAllByTeam(Team team) {
        return profileRepository.findAllByTeam(team);
    }

    @Override
    public boolean existsByTeamAndUser_UserType(Team team, UserType userType) {
        return profileRepository.existsByTeamAndUser_UserType(team, userType);
    }

    @Override
    @Transactional
    public void deleteByUser_Id(Long id) {
        profileRepository.deleteByUser_Id(id);
    }

    @Override
    public Profile findByTeamAndUser_UserType(Team team, UserType userType) {
        return profileRepository.findByTeamAndUser_UserType(team,userType);
    }


    private void validateProfile(Profile profile) {
        verifications.verify(new VerifyNullorEmptyCommand(profile.getFirstName(),
                message.getMessage("NotNull.profile.firstName.message")));
        verifications.verify(new VerifyNullorEmptyCommand(profile.getLastName(),
                message.getMessage("NotNull.profile.lastName.message")));
        verifications.verify(new VerifyNullorEmptyCommand(profile.getNickName(),
                message.getMessage("NotNull.profile.nickname.message")));
        verifications.verify(new VerifyDateCommand(profile.getBirthdate(),
                message.getMessage("NotNull.profile.birthDate.message")));
        verifications.verify(new VerifyIntegerNumbersCommand(profile.getTeam().getId(),
                message.getMessage("NotNull.profile.idTeam.message")));

    }
}
