package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.Team;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
public interface IProfileService {
    Profile save(Profile profile);

    Profile findByUser_Id(Long userId);

    boolean existsByUser_Id(Long userId);

    List<Profile> findAllByTeam(Team team);

    boolean existsByTeamAndUser_UserType(Team team, UserType userType);

    void deleteByUser_Id(Long id);

    Profile findByTeamAndUser_UserType(Team team, UserType userType);
}
