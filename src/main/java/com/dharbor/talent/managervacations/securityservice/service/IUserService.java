package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.domain.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
public interface IUserService {
    User save(User user);

    void deleteUserById(Long id);

    boolean existUserbyId(Long id);

    Optional<User> findById(Long id);

    User findByEmail(String email);
}
