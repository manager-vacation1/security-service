package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class SubtractNumberOffSetsUseCase {

    private IProfileService iProfileService;

    public void execute(Long userId) {
        Profile user = iProfileService.findByUser_Id(userId);
        short compensationDaysUser = user.getNumberOfOffsets();
        short totalCompensationUser = --compensationDaysUser;
        user.setNumberOfOffsets(totalCompensationUser);
        iProfileService.save(user);
    }
}
