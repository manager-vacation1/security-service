package com.dharbor.talent.managervacations.securityservice.mapper;

import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.dto.TeamDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface TeamStructMapper {
    TeamDto teamToTeamDto(Team team);
    List<TeamDto> listTeamtoListTeamDto(List<Team> list);
}
