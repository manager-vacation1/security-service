package com.dharbor.talent.managervacations.securityservice.dto.response.user;

import com.dharbor.talent.managervacations.securityservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.securityservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.securityservice.dto.UserDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class UserResponse extends CommonResponseDomain {

    private UserDto userDto;

    public UserResponse(UserDto userDto) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.userDto = userDto;
    }
}
