package com.dharbor.talent.managervacations.securityservice.dto.response.user;

import com.dharbor.talent.managervacations.securityservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.securityservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListUserAndProfileResponse extends CommonResponseDomain {

    private List<ProfileDto> listTeam;

    public ListUserAndProfileResponse(List<ProfileDto> listTeam) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.listTeam = listTeam;
    }
}
