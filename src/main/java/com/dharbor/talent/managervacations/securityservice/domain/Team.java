package com.dharbor.talent.managervacations.securityservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@Entity
@Table(name = ConstantsNamesTableBd.TeamsTable.NAME)
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ConstantsNamesTableBd.TeamsTable.Id.NAME)
    private Integer id;

    @Column(name = ConstantsNamesTableBd.TeamsTable.Name.NAME,
            length = ConstantsNamesTableBd.TeamsTable.Name.LENGTH)
    private String teamName;

}
