package com.dharbor.talent.managervacations.securityservice.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Jhonatan Soto
 */
@FeignClient(name = "country-service")
public interface CountryClient {

    @GetMapping(value = "/countries/{countryId}")
    ResponseEntity<CountryResponse> getCountryById(@PathVariable(value = "countryId") Long countryId);

}
