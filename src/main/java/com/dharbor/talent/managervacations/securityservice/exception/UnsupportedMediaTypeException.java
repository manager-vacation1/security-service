package com.dharbor.talent.managervacations.securityservice.exception;

/**
 * @author Jhonatan Soto
 */
public class UnsupportedMediaTypeException extends RuntimeException {
    public UnsupportedMediaTypeException(String message) {
        super(message);
    }
}
