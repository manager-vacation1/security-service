package com.dharbor.talent.managervacations.securityservice.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CommonResponseDomain {
    private String statusCode;
    private String message;
}
