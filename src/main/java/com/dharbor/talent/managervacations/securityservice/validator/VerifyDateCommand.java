package com.dharbor.talent.managervacations.securityservice.validator;

import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
public class VerifyDateCommand implements Command {

    private final boolean validate;
    private final String message;

    public VerifyDateCommand(Date stringToVerify, String message) {
        this.validate = stringToVerify == null;
        this.message = message;
    }

    @Override
    public void execute() {
        if (validate) {
            throw new BadRequestException(message);
        }
    }
}
