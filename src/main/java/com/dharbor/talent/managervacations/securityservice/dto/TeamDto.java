package com.dharbor.talent.managervacations.securityservice.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class TeamDto {

    private Integer id;

    private String teamName;
}
