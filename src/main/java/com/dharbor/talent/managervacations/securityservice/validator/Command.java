package com.dharbor.talent.managervacations.securityservice.validator;

/**
 * @author Jhonatan Soto
 */
public interface Command {
    void execute();
}
