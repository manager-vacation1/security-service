package com.dharbor.talent.managervacations.securityservice.dto.response.user;

import com.dharbor.talent.managervacations.securityservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.securityservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAndProfileResponse extends CommonResponseDomain {

    private ProfileDto profile;

    public UserAndProfileResponse(ProfileDto profile) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.profile = profile;
    }
}
