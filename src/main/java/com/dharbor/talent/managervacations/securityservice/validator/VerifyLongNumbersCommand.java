package com.dharbor.talent.managervacations.securityservice.validator;

import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;

/**
 * @author Jhonatan Soto
 */
public class VerifyLongNumbersCommand implements Command {
    private final boolean validate;
    private final String message;

    public VerifyLongNumbersCommand(Long number, String message) {
        this.validate = number == null || number <= 0;
        this.message = message;
    }
    @Override
    public void execute() {
        if(validate){
            throw new BadRequestException(message);
        }
    }
}
