package com.dharbor.talent.managervacations.securityservice.config;

import com.dharbor.talent.managervacations.securityservice.validator.Verifications;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jhonatan Soto
 */
@Configuration
public class VerifyConfig {
    @Bean
    public Verifications verificators() {
        return new Verifications();
    }
}
