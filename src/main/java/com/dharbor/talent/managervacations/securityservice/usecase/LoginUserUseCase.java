package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.dto.TokenDto;
import com.dharbor.talent.managervacations.securityservice.dto.UserTokenDto;
import com.dharbor.talent.managervacations.securityservice.dto.request.UserRequest;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.LoginResponse;
import com.dharbor.talent.managervacations.securityservice.mapper.TokenStructMapper;
import com.dharbor.talent.managervacations.securityservice.mapper.UserStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import com.dharbor.talent.managervacations.securityservice.service.IUserService;
import com.dharbor.talent.managervacations.securityservice.service.KeyCloakService;
import lombok.AllArgsConstructor;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class LoginUserUseCase {

    private KeyCloakService keyCloakService;
    private TokenStructMapper tokenStructMapper;
    private IUserService iUserService;
    private IProfileService iProfileService;
    private UserStructMapper userStructMapper;


    public LoginResponse execute(UserRequest userRequest) {
        User user = iUserService.findByEmail(userRequest.getEmail());
        Profile profile = iProfileService.findByUser_Id(user.getId());
        UserTokenDto userTokenDto = userStructMapper.userToUserTokenDto(user);
        AccessTokenResponse accessTokenResponse = keyCloakService.login(userRequest);
        TokenDto tokenDto = tokenStructMapper.tokenToTokenDto(accessTokenResponse);

        userTokenDto.setNickName(profile.getNickName());

        return new LoginResponse(tokenDto, userTokenDto);
    }
}
