package com.dharbor.talent.managervacations.securityservice.repository;

import com.dharbor.talent.managervacations.securityservice.domain.Team;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Jhonatan Soto
 */
public interface TeamRepository extends JpaRepository<Team, Integer> {
    boolean existsByTeamName(String teamName);
}
