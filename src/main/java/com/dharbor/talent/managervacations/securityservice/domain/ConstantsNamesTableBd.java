package com.dharbor.talent.managervacations.securityservice.domain;

/**
 * @author Jhonatan Sotoo
 */
final class ConstantsNamesTableBd {
    static class UsersTable {
        static final String NAME = "users_table";

        static class Id {
            static final String NAME = "users_id";
        }

        static class Email {
            static final String NAME = "users_email";
            static final int LENGTH = 100;
        }

        static class Password {
            static final String NAME = "users_password";
            static final int LENGTH = 200;
        }

        static class UserType {
            static final String NAME = "users_type";
            static final int LENGTH = 50;
        }

        static class CreatedDate {
            static final String NAME = "users_created_date";
        }
    }

    static class TeamsTable {
        static final String NAME = "teams_table";

        static class Id {
            static final String NAME = "teams_id";
        }

        static class Name {
            static final String NAME = "teams_name";
            static final int LENGTH = 100;
        }
    }

    static class ProfileTable {
        static final String NAME = "profiles_table";

        static class Id {
            static final String NAME = "profiles_id";
        }

        static class FirstName {
            static final String NAME = "profiles_first_name";
        }

        static class LastName {
            static final String NAME = "profiles_last_name";
        }

        static class Birthdate {
            static final String NAME = "profiles_birthdate";
        }

        static class NickName {
            static final String NAME = "profiles_nick_name";
        }

        static class ImageMongoId {
            static final String NAME = "profiles_image_mongo_id";
        }

        static class Country {
            static final String NAME = "profiles_country_id";
        }

        static class Offsets {
            static final String NAME = "profiles_number_offsets";
        }

        static class Team {
            static final String NAME = "profiles_team_id";
        }

        static class User {
            static final String NAME = "profiles_user_id";
        }
    }
}
