package com.dharbor.talent.managervacations.securityservice.mapper;

import com.dharbor.talent.managervacations.securityservice.dto.TokenDto;
import org.keycloak.representations.AccessTokenResponse;
import org.mapstruct.Mapper;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface TokenStructMapper {

    TokenDto tokenToTokenDto(AccessTokenResponse accessTokenResponse);

}
