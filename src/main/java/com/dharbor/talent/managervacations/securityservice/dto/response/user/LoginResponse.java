package com.dharbor.talent.managervacations.securityservice.dto.response.user;

import com.dharbor.talent.managervacations.securityservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.securityservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.securityservice.dto.TokenDto;
import com.dharbor.talent.managervacations.securityservice.dto.UserTokenDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class LoginResponse extends CommonResponseDomain {

    private TokenDto accessTokenResponse;
    private UserTokenDto userTokenDto;

    public LoginResponse(TokenDto accessTokenResponse, UserTokenDto userTokenDto) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.accessTokenResponse = accessTokenResponse;
        this.userTokenDto = userTokenDto;
    }
}
