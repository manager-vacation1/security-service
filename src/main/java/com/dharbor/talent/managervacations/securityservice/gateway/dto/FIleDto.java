package com.dharbor.talent.managervacations.securityservice.gateway.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class FIleDto {

    private String id;

    private String mimeType;

    private String name;

    private Long size;

}
