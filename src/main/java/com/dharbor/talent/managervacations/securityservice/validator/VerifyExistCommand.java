package com.dharbor.talent.managervacations.securityservice.validator;

import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;

/**
 * @author Jhonatan Soto
 */
public class VerifyExistCommand implements Command {

    private final String message;
    private final boolean validate;

    public VerifyExistCommand(String message, boolean validate) {
        this.message = message;
        this.validate = validate;
    }

    @Override
    public void execute() {
        if (validate) {
            throw new BadRequestException(message);
        }
    }
}
