package com.dharbor.talent.managervacations.securityservice.usecase.profile;

import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.mapper.ProfileStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetProfileByUserIdUseCase {

    private IProfileService iProfileService;

    private ProfileStructMapper profileStructMapper;

    public UserAndProfileResponse execute(Long id) {
        Profile profile = iProfileService.findByUser_Id(id);
        ProfileDto profileDto = profileStructMapper.profileToProfileDto(profile);
        return new UserAndProfileResponse(profileDto);
    }

}
