package com.dharbor.talent.managervacations.securityservice.gateway;

import com.dharbor.talent.managervacations.securityservice.gateway.dto.FIleDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Jhonatan Soto
 */
@FeignClient(name = "gallery-service")
@Service
public interface GalleryClient {

    @PostMapping(value = "/gallery",consumes = "multipart/form-data")
    FIleDto uploadFile(@RequestPart MultipartFile file);
}
