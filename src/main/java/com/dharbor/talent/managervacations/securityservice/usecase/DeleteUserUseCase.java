package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import com.dharbor.talent.managervacations.securityservice.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class DeleteUserUseCase {


    private IProfileService iProfileService;

    public void execute(Long id) {
        iProfileService.deleteByUser_Id(id);
    }
}
