package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.domain.Team;

import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
public interface ITeamService {
    Team save(Team team);
    Optional<Team> findById(Integer id);
    List<Team> findAll();
}
