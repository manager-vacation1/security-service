package com.dharbor.talent.managervacations.securityservice.mapper;

import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.dto.UserDto;
import com.dharbor.talent.managervacations.securityservice.dto.UserTokenDto;
import org.mapstruct.Mapper;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface UserStructMapper {

    UserDto userToUserDto(User user);

    UserTokenDto userToUserTokenDto(User user);

}

