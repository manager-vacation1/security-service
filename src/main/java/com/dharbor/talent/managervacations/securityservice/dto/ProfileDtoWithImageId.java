package com.dharbor.talent.managervacations.securityservice.dto;

import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class ProfileDtoWithImageId {

    private Long id;

    private String firstName;

    private String lastName;

    private Date birthdate;

    private String nickName;

    private Long idCountry;

    private Team team;

    private String imageMongoId;

    private User user;

}
