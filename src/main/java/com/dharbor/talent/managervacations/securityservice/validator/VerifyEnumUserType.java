package com.dharbor.talent.managervacations.securityservice.validator;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;

/**
 * @author Jhonatan Soto
 */
public class VerifyEnumUserType implements Command {
    private final boolean validate;
    private final String message;

    public VerifyEnumUserType(UserType userType, String message) {
        this.validate = UserType.findByType(userType);
        this.message = message;
    }

    @Override
    public void execute() {
        if (!validate) {
            throw new BadRequestException(message);
        }
    }
}
