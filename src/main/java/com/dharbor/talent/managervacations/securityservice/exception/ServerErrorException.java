package com.dharbor.talent.managervacations.securityservice.exception;

/**
 * @author Jhonatan Soto
 */
public class ServerErrorException extends RuntimeException {
    public ServerErrorException(String message) {
        super(message);
    }
}
