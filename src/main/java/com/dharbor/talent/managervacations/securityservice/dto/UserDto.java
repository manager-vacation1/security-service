package com.dharbor.talent.managervacations.securityservice.dto;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@AllArgsConstructor
public class UserDto {

    private Long id;

    private String email;

    private UserType userType;

    private Date createdDate;

}
