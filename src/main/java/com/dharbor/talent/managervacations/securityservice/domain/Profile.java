package com.dharbor.talent.managervacations.securityservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import static com.dharbor.talent.managervacations.securityservice.domain.ConstantsNamesTableBd.ProfileTable;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@Entity
@Table(name = ProfileTable.NAME)
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ProfileTable.Id.NAME)
    private Long id;

    @Column(name = ProfileTable.FirstName.NAME, nullable = false)
    private String firstName;

    @Column(name = ProfileTable.LastName.NAME, nullable = false)
    private String lastName;

    @Temporal(TemporalType.DATE)
    @Column(name = ProfileTable.Birthdate.NAME, nullable = false)
    private Date birthdate;

    @Column(name = ProfileTable.NickName.NAME, nullable = false)
    private String nickName;

    @Column(name = ProfileTable.ImageMongoId.NAME)
    private String imageMongoId;

    @Column(name = ProfileTable.Country.NAME, nullable = false)
    private Long idCountry;

    @Column(name = ProfileTable.Offsets.NAME)
    private Short numberOfOffsets;

    @JoinColumn(name = ProfileTable.Team.NAME,
            referencedColumnName = ConstantsNamesTableBd.TeamsTable.Id.NAME, nullable = false)
    @ManyToOne
    private Team team;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = ProfileTable.User.NAME,
            referencedColumnName = ConstantsNamesTableBd.UsersTable.Id.NAME, nullable = false)
    private User user;

    @PrePersist
    void onPrePersist() {
        this.numberOfOffsets = 0;
//        Default Image, Image In MongoDb
        this.imageMongoId = "6244a5d8cc4b3e23cd0673e3";
    }
}
