package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.securityservice.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.securityservice.repository.UserRepository;
import com.dharbor.talent.managervacations.securityservice.validator.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements IUserService {

    private UserRepository userRepository;

    private Verifications verifications;

    private Message message;

    @Override
    public User save(User user) {
        validateUser(user);
        return userRepository.save(user);
    }

    @Override
    public void deleteUserById(Long id) {
        verifications.verify(new VerifyExistUserCommand(message.getMessage("Exist.profile.userId.message"),
                userRepository.existsById(id)));
        userRepository.deleteById(id);
    }

    @Override
    public boolean existUserbyId(Long id) {
        return userRepository.existsById(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.ofNullable(userRepository.findById(id).orElseThrow(() ->
                new ResourceNotFound(message.getMessage("Exist.profile.userId.message"))));
    }

    @Override
    public User findByEmail(String email) {
        verifications.verify(new VerifyExistCommand(
                message.getMessage("No.Exist.email.userId.message"),
                !userRepository.existsByEmail(email)));
        return userRepository.findByEmail(email);
    }


    private void validateUser(User user) {
        verifications.verify(new VerifyNullorEmptyCommand(user.getEmail(),
                message.getMessage("NotNull.user.email.message")));
        verifications.verify(new VerifyNullorEmptyCommand(user.getPassword(),
                message.getMessage("NotNull.user.password.message")));
        verifications.verify(new VerifyDateCommand(user.getCreatedDate(), message
                .getMessage("NotNull.user.date.message")));
        verifications.verify(new VerifyEnumUserType(user.getUserType(), message
                .getMessage("NoExist.user.user.type.message")));
        verifications.verify(new VerifyExistCommand(
                message.getMessage("Exist.user.email.message"),
                userRepository.existsByEmail(user.getEmail())));
    }
}
