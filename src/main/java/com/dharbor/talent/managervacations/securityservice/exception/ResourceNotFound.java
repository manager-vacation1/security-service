package com.dharbor.talent.managervacations.securityservice.exception;

/**
 * @author Jhonatan Soto
 */
public class ResourceNotFound extends RuntimeException {
    public ResourceNotFound(String message) {
        super(message);
    }
}
