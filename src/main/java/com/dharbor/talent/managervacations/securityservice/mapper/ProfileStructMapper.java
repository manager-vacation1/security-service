package com.dharbor.talent.managervacations.securityservice.mapper;

import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface ProfileStructMapper {
    ProfileDto profileToProfileDto(Profile profile);

    List<ProfileDto> profileListtoProfileDtio(List<Profile> profileList);

}
