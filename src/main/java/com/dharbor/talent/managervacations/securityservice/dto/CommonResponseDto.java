package com.dharbor.talent.managervacations.securityservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CommonResponseDto {
    private String statusCode;
    private String message;
}
