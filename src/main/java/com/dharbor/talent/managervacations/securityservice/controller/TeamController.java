package com.dharbor.talent.managervacations.securityservice.controller;

import com.dharbor.talent.managervacations.securityservice.dto.request.TeamRequest;
import com.dharbor.talent.managervacations.securityservice.dto.response.team.ListTeamResponse;
import com.dharbor.talent.managervacations.securityservice.dto.response.team.TeamResponse;
import com.dharbor.talent.managervacations.securityservice.usecase.team.CreateTeamUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.team.GetAllTeamUseCase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Soto
 */
@RestController
@RequestMapping("/team")
@AllArgsConstructor
public class TeamController {

    private CreateTeamUseCase createTeamUseCase;

    private GetAllTeamUseCase getAllTeamUseCase;


    @PostMapping
    public TeamResponse saveTeam(@RequestBody TeamRequest teamRequest) {
        return createTeamUseCase.execute(teamRequest);
    }

    @GetMapping("/all")
    public ListTeamResponse getAllTeams() {
        return getAllTeamUseCase.execute();
    }


}
