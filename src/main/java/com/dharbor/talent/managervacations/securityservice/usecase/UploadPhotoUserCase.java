package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.securityservice.exception.UnsupportedMediaTypeException;
import com.dharbor.talent.managervacations.securityservice.gateway.GalleryClient;
import com.dharbor.talent.managervacations.securityservice.gateway.dto.FIleDto;
import com.dharbor.talent.managervacations.securityservice.mapper.ProfileStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class UploadPhotoUserCase {
    private IProfileService iProfileService;

    private GalleryClient galleryClient;

    private ProfileStructMapper profileStructMapper;

    private Message message;

    public UserAndProfileResponse execute(Long id, MultipartFile multipartFile) {
        validateMultipartFileIsImage(multipartFile);
        validateUser(id);
        FIleDto fileResponse = galleryClient.uploadFile(multipartFile);
        Profile profile = iProfileService.findByUser_Id(id);
        profile.setImageMongoId(fileResponse.getId());
        iProfileService.save(profile);
        ProfileDto profileDto = profileStructMapper.profileToProfileDto(profile);
        return new UserAndProfileResponse(profileDto);
    }

    private void validateMultipartFileIsImage(MultipartFile multipartFile) {
        if (!multipartFile.getContentType().contains("image/")) {
            throw new UnsupportedMediaTypeException(message.getMessage(
                    "Unssupported.Media.Type") + ": " + multipartFile.getContentType());
        }
    }

    private void validateUser(Long id) {
        if (!iProfileService.existsByUser_Id(id)) {
            throw new ResourceNotFound(message.getMessage("Exist.profile.userId.message"));
        }
    }

}
