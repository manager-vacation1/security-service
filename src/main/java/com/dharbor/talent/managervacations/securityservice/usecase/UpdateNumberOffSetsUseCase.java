package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class UpdateNumberOffSetsUseCase {

    private IProfileService iProfileService;

    public void execute(Long userId, Short numberOfOffsets) {
        Profile user = iProfileService.findByUser_Id(userId);
        user.setNumberOfOffsets((short) (user.getNumberOfOffsets() + numberOfOffsets));
        iProfileService.save(user);
    }
}
