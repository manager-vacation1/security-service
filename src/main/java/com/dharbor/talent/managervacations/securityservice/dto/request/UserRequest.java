package com.dharbor.talent.managervacations.securityservice.dto.request;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserRequest {

    private String email;

    private String password;

    private UserType userType;

    private Date createdDate;

    /*Data Profile*/
    private String firstName;

    private String lastName;

    private Date birthdate;

    private String nickName;

    private Long idCountry;

    private Integer idTeam;

}
