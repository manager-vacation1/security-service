package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import com.dharbor.talent.managervacations.securityservice.dto.request.UserRequest;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.securityservice.exception.ServerErrorException;
import com.dharbor.talent.managervacations.securityservice.gateway.CountryClient;
import com.dharbor.talent.managervacations.securityservice.mapper.ProfileStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import com.dharbor.talent.managervacations.securityservice.service.ITeamService;
import com.dharbor.talent.managervacations.securityservice.service.IUserService;
import com.dharbor.talent.managervacations.securityservice.service.KeyCloakService;
import com.dharbor.talent.managervacations.securityservice.validator.Verifications;
import com.dharbor.talent.managervacations.securityservice.validator.VerifyExistCommand;
import com.dharbor.talent.managervacations.securityservice.validator.VerifyLongNumbersCommand;
import feign.FeignException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class CreateUserUseCase {

    private IUserService iUserService;

    private IProfileService iProfileService;

    private ITeamService iTeamService;

    private ProfileStructMapper profileStructMapper;

    private CountryClient countryClient;

    private Message message;

    private Verifications verifications;

    private KeyCloakService keyCloakService;


    public UserAndProfileResponse execute(UserRequest userRequest) {
        validateCountryIdExist(userRequest.getIdCountry());
        Team team = iTeamService.findById(userRequest.getIdTeam())
                .orElseThrow(() -> new ResourceNotFound(message.getMessage("Team.Not.Found.message")));
        validateManagerTeam(team, userRequest.getUserType());
        User user = iUserService.save(buildUser(userRequest));
        Profile profile = iProfileService.save(buildProfile(userRequest, user, team));
        keyCloakService.createUser(userRequest);
        return builSavedUserResponse(profile);
    }

    private UserAndProfileResponse builSavedUserResponse(Profile profile) {
        ProfileDto profileDto = profileStructMapper.profileToProfileDto(profile);
        return new UserAndProfileResponse(profileDto);
    }

    private Profile buildProfile(UserRequest userRequest, User user, Team team) {

        Profile buildProfile = new Profile();
        buildProfile.setFirstName(userRequest.getFirstName());
        buildProfile.setLastName(userRequest.getLastName());
        buildProfile.setBirthdate(userRequest.getBirthdate());
        buildProfile.setNickName(userRequest.getNickName());
        buildProfile.setIdCountry(userRequest.getIdCountry());
        buildProfile.setTeam(team);
        buildProfile.setUser(user);
        return buildProfile;
    }

    private User buildUser(UserRequest userRequest) {
        User buildUser = new User();
        buildUser.setEmail(userRequest.getEmail());
        buildUser.setUserType(userRequest.getUserType());
        buildUser.setCreatedDate(userRequest.getCreatedDate());
        buildUser.setPassword(userRequest.getPassword());
        return buildUser;
    }

    private void validateManagerTeam(Team team, UserType userType) {
        if (iProfileService.existsByTeamAndUser_UserType(team, UserType.MANAGER) &&
                userType.equals(UserType.MANAGER)) {
            verifications.verify(new VerifyExistCommand(
                    message.getMessage("Exist.Team.Manager"),
                    iProfileService.existsByTeamAndUser_UserType(team, UserType.MANAGER)));
        }
    }

    private void validateCountryIdExist(Long idCountry) {
        verifications.verify(new VerifyLongNumbersCommand(idCountry,
                message.getMessage("NotNull.profile.idCountry.message")));
        try {
            countryClient.getCountryById(idCountry);
        } catch (FeignException e) {
            if (e.getMessage().contains("1011")) {
                throw new ResourceNotFound(message.getMessage("Not.Exist.country.idTeam.message"));
            } else {
                throw new ServerErrorException(message.getMessage("Problem.Server.country.idTeam.message"));
            }

        }
    }

}
