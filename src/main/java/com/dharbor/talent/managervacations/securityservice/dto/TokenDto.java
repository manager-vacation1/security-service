package com.dharbor.talent.managervacations.securityservice.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class TokenDto {

    private String token;
    private long expiresIn;
    private long refreshExpiresIn;
    private String refreshToken;
}
