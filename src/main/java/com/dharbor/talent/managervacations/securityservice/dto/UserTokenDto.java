package com.dharbor.talent.managervacations.securityservice.dto;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class UserTokenDto {
    private Long id;
    private String email;
    private UserType userType;
    private String nickName;

}
