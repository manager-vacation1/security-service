package com.dharbor.talent.managervacations.securityservice.constant;

public enum UserType {
    MANAGER, COLLABORATOR;

    public static boolean findByType(UserType userType) {
        for (UserType v : UserType.values()) {
            if(v.equals(userType)){
                return true;
            }
        }
        return false;
    }
}