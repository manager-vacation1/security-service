package com.dharbor.talent.managervacations.securityservice.gateway;

import com.dharbor.talent.managervacations.securityservice.dto.CommonResponseDto;
import com.dharbor.talent.managervacations.securityservice.gateway.dto.CountryDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class CountryResponse extends CommonResponseDto {
    private CountryDto country;
}
