package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.securityservice.repository.TeamRepository;
import com.dharbor.talent.managervacations.securityservice.validator.Verifications;
import com.dharbor.talent.managervacations.securityservice.validator.VerifyIntegerNumbersCommand;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class TeamServiceImpl implements ITeamService {

    private TeamRepository teamRepository;

    private Verifications verifications;

    private Message message;

    @Override
    public Team save(Team team) {
        if (teamRepository.existsByTeamName(team.getTeamName())) {
            throw new BadRequestException(message.getMessage("Exist.Team.Name"));
        }
        return teamRepository.save(team);
    }

    @Override
    public Optional<Team> findById(Integer id) {
        verifications.verify(new VerifyIntegerNumbersCommand(id,
                message.getMessage("NotNull.Team.name.message")));
        return teamRepository.findById(id);
    }

    @Override
    public List<Team> findAll() {
        return teamRepository.findAll();
    }

}
