package com.dharbor.talent.managervacations.securityservice.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class TeamRequest {
    private String teamName;
}
