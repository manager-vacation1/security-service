package com.dharbor.talent.managervacations.securityservice.domain;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.Date;
import java.util.TimeZone;


@Getter
@Setter
@Entity
@Table(name = ConstantsNamesTableBd.UsersTable.NAME)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ConstantsNamesTableBd.UsersTable.Id.NAME)
    private Long id;

    @Column(name = ConstantsNamesTableBd.UsersTable.Email.NAME,
            length = ConstantsNamesTableBd.UsersTable.Email.LENGTH, nullable = false, unique = true)
    private String email;

    @Column(name = ConstantsNamesTableBd.UsersTable.Password.NAME,
            length = ConstantsNamesTableBd.UsersTable.Password.LENGTH, nullable = false)

    @JsonIgnore
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = ConstantsNamesTableBd.UsersTable.UserType.NAME,
            length = ConstantsNamesTableBd.UsersTable.UserType.LENGTH, nullable = false)
    private UserType userType;

    @Temporal(TemporalType.DATE)
    @Column(name = ConstantsNamesTableBd.UsersTable.CreatedDate.NAME, nullable = false)
    private Date createdDate;
}
