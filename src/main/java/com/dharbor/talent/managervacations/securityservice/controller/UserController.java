package com.dharbor.talent.managervacations.securityservice.controller;

import com.dharbor.talent.managervacations.securityservice.dto.request.UserRequest;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.LoginResponse;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserResponse;
import com.dharbor.talent.managervacations.securityservice.service.KeyCloakService;
import com.dharbor.talent.managervacations.securityservice.usecase.*;
import lombok.AllArgsConstructor;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Jhonatan Soto
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private CreateUserUseCase createUserUseCase;

    private UploadPhotoUserCase uploadPhotoUserCase;

    private DeleteUserUseCase deleteUserUseCase;

    private GetUserByIdUseCase getUserByIdUseCase;

    private LoginUserUseCase loginUserUseCase;


    @GetMapping("/{userId}")
    public UserResponse getUserById(@PathVariable Long userId) {
        return getUserByIdUseCase.execute(userId);
    }

    @PostMapping("/create")
    public UserAndProfileResponse savedUser(@RequestBody UserRequest userRequest) {
        return createUserUseCase.execute(userRequest);
    }

    @PutMapping(value = "/update/photo/{userId}", consumes = "multipart/form-data")
    public UserAndProfileResponse updatePhoto(@RequestPart MultipartFile multipartFile,
                                              @PathVariable Long userId) {
        return uploadPhotoUserCase.execute(userId, multipartFile);
    }

    @DeleteMapping("/delete/{userId}")
    public void deleteUser(@PathVariable Long userId) {
        deleteUserUseCase.execute(userId);
    }

    @PostMapping(path = "/login")
    public ResponseEntity<LoginResponse> login(@RequestBody UserRequest userRequest) {
        LoginResponse loginResponse = loginUserUseCase.execute(userRequest);
        return ResponseEntity.ok(loginResponse);
    }
}
