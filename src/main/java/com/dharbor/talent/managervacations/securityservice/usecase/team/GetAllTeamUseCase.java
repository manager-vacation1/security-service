package com.dharbor.talent.managervacations.securityservice.usecase.team;

import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.dto.response.team.ListTeamResponse;
import com.dharbor.talent.managervacations.securityservice.mapper.TeamStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.ITeamService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetAllTeamUseCase {

    private ITeamService iTeamService;

    private TeamStructMapper teamStructMapper;

    public ListTeamResponse execute() {
        List<Team> listTeams = iTeamService.findAll();
        return new ListTeamResponse(teamStructMapper.listTeamtoListTeamDto(listTeams));
    }
}
