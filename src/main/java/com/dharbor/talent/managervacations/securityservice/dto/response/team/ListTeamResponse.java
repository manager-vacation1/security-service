package com.dharbor.talent.managervacations.securityservice.dto.response.team;

import com.dharbor.talent.managervacations.securityservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.securityservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.securityservice.dto.TeamDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class ListTeamResponse extends CommonResponseDomain {

    private List<TeamDto> teams;

    public ListTeamResponse(List<TeamDto> teams) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.teams = teams;
    }
}
