package com.dharbor.talent.managervacations.securityservice.usecase;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserResponse;
import com.dharbor.talent.managervacations.securityservice.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.securityservice.mapper.UserStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetUserByIdUseCase {

    private IUserService iUserService;

    private UserStructMapper userStructMapper;

    private Message message;

    public UserResponse execute(Long id) {
        Optional<User> user = Optional.ofNullable(iUserService.findById(id)
                .orElseThrow(() -> new ResourceNotFound(message.getMessage("Exist.profile.userId.message"))));
        return new UserResponse(userStructMapper.userToUserDto(user.get()));
    }
}
