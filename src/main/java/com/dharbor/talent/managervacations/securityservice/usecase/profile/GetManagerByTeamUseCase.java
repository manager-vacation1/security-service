package com.dharbor.talent.managervacations.securityservice.usecase.profile;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.dto.UserDto;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserResponse;
import com.dharbor.talent.managervacations.securityservice.mapper.UserStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.IProfileService;
import com.dharbor.talent.managervacations.securityservice.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetManagerByTeamUseCase {

    private IProfileService iProfileService;

    private UserStructMapper userStructMapper;

    public UserResponse execute(Long id) {
        Profile profileUser = iProfileService.findByUser_Id(id);
        Profile profileManager = iProfileService.findByTeamAndUser_UserType(profileUser.getTeam(), UserType.MANAGER);
        UserDto userDto = userStructMapper.userToUserDto(profileManager.getUser());
        return new UserResponse(userDto);
    }
}
