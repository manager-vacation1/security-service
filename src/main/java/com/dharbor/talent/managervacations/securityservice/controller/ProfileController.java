package com.dharbor.talent.managervacations.securityservice.controller;

import com.dharbor.talent.managervacations.securityservice.dto.response.user.ListUserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserResponse;
import com.dharbor.talent.managervacations.securityservice.usecase.SubtractNumberOffSetsUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.UpdateNumberOffSetsUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.profile.GetManagerByTeamUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.profile.GetProfileByUserIdUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.profile.GetTeamsByUserIdUseCase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Soto
 */
@RestController
@RequestMapping("/profile")
@AllArgsConstructor
public class ProfileController {

    private GetProfileByUserIdUseCase getProfileByUserIdUseCase;

    private GetTeamsByUserIdUseCase getTeamsByUserIdUseCase;

    private UpdateNumberOffSetsUseCase updateNumberOffSetsUseCase;

    private GetManagerByTeamUseCase getManagerByTeamUseCase;

    private SubtractNumberOffSetsUseCase subtractNumberOffSetsUseCase;


    @GetMapping("/{userId}")
    public UserAndProfileResponse getProfileByUserId(@PathVariable Long userId) {
        return getProfileByUserIdUseCase.execute(userId);
    }

    @GetMapping("/team/members/{userId}")
    public ListUserAndProfileResponse getTeamMembersByUserId(@PathVariable Long userId) {
        return getTeamsByUserIdUseCase.execute(userId);
    }

    @PutMapping("/update/numberOfOffsets")
    public void updateNumberOfOffsets(@RequestParam Long userId, @RequestParam Short numberOfOffsets) {
        updateNumberOffSetsUseCase.execute(userId, numberOfOffsets);
    }

    @PutMapping("/subtract/numberOfOffsets/{userId}")
    public void subtractNumberOfOffsets(@PathVariable Long userId) {
        subtractNumberOffSetsUseCase.execute(userId);
    }


    @GetMapping("/get/manager/{userId}")
    public UserResponse getManagerByUserId(@PathVariable Long userId) {
        return getManagerByTeamUseCase.execute(userId);
    }
}
