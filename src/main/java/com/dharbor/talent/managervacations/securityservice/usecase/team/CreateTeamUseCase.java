package com.dharbor.talent.managervacations.securityservice.usecase.team;

import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.dto.request.TeamRequest;
import com.dharbor.talent.managervacations.securityservice.dto.response.team.TeamResponse;
import com.dharbor.talent.managervacations.securityservice.mapper.TeamStructMapper;
import com.dharbor.talent.managervacations.securityservice.service.ITeamService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
@Slf4j
public class CreateTeamUseCase {

    private ITeamService iTeamService;

    private TeamStructMapper teamStructMapper;

    public TeamResponse execute(TeamRequest teamRequest) {
        Team team = iTeamService.save(buildTeam(teamRequest));
        return new TeamResponse(teamStructMapper.teamToTeamDto(team));
    }

    private Team buildTeam(TeamRequest teamRequest) {
        Team team = new Team();
        team.setTeamName(teamRequest.getTeamName());
        return team;
    }
}
