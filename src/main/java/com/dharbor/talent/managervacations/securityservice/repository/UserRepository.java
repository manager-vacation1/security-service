package com.dharbor.talent.managervacations.securityservice.repository;

import com.dharbor.talent.managervacations.securityservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);

    boolean existsById(Long id);

    User findByEmail(String email);
}
