package com.dharbor.talent.managervacations.securityservice.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.dto.ProfileDto;
import com.dharbor.talent.managervacations.securityservice.dto.request.UserRequest;
import com.dharbor.talent.managervacations.securityservice.dto.response.user.UserAndProfileResponse;
import com.dharbor.talent.managervacations.securityservice.service.KeyCloakService;
import com.dharbor.talent.managervacations.securityservice.usecase.CreateUserUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.DeleteUserUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.GetUserByIdUseCase;
import com.dharbor.talent.managervacations.securityservice.usecase.UploadPhotoUserCase;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {UserController.class})
@ExtendWith(SpringExtension.class)
class UserControllerTest {
    @MockBean
    private CreateUserUseCase createUserUseCase;

    @MockBean
    private DeleteUserUseCase deleteUserUseCase;

    @MockBean
    private GetUserByIdUseCase getUserByIdUseCase;

    @MockBean
    private KeyCloakService keyCloakService;

    @MockBean
    private UploadPhotoUserCase uploadPhotoUserCase;

    @Autowired
    private UserController userController;

    @Test
    void testSavedUser() throws Exception {
        when(this.createUserUseCase.execute((UserRequest) any())).thenReturn(new UserAndProfileResponse(new ProfileDto()));

        UserRequest userRequest = new UserRequest();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        userRequest.setBirthdate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        userRequest.setCreatedDate(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setFirstName("Jane");
        userRequest.setIdCountry(3L);
        userRequest.setIdTeam(1);
        userRequest.setLastName("Doe");
        userRequest.setNickName("Nick Name");
        userRequest.setPassword("iloveyou");
        userRequest.setUserType(UserType.MANAGER);
        String content = (new ObjectMapper()).writeValueAsString(userRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"statusCode\":\"200\",\"message\":\"Success\",\"profile\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"birthdate"
                                        + "\":null,\"nickName\":null,\"numberOfOffsets\":null,\"imageMongoId\":null,\"idCountry\":null,\"team\":null,\"user"
                                        + "\":null}}"));
    }

    @Test
    void testSavedUser2() throws Exception {
        when(this.createUserUseCase.execute((UserRequest) any())).thenReturn(new UserAndProfileResponse(new ProfileDto()));

        UserRequest userRequest = new UserRequest();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        userRequest.setBirthdate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        userRequest.setCreatedDate(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setFirstName("Jane");
        userRequest.setIdCountry(3L);
        userRequest.setIdTeam(1);
        userRequest.setLastName("Doe");
        userRequest.setNickName("Nick Name");
        userRequest.setPassword("iloveyou");
        userRequest.setUserType(UserType.MANAGER);
        String content = (new ObjectMapper()).writeValueAsString(userRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"statusCode\":\"200\",\"message\":\"Success\",\"profile\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"birthdate"
                                        + "\":null,\"nickName\":null,\"numberOfOffsets\":null,\"imageMongoId\":null,\"idCountry\":null,\"team\":null,\"user"
                                        + "\":null}}"));
    }

    @Test
    void testSavedUser3() throws Exception {
        when(this.createUserUseCase.execute((UserRequest) any())).thenReturn(new UserAndProfileResponse(new ProfileDto()));

        UserRequest userRequest = new UserRequest();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        userRequest.setBirthdate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        userRequest.setCreatedDate(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        userRequest.setEmail("jane.doe@example.org");
        userRequest.setFirstName("Jane");
        userRequest.setIdCountry(3L);
        userRequest.setIdTeam(1);
        userRequest.setLastName("Doe");
        userRequest.setNickName("Nick Name");
        userRequest.setPassword("iloveyou");
        userRequest.setUserType(UserType.MANAGER);
        String content = (new ObjectMapper()).writeValueAsString(userRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"statusCode\":\"200\",\"message\":\"Success\",\"profile\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"birthdate"
                                        + "\":null,\"nickName\":null,\"numberOfOffsets\":null,\"imageMongoId\":null,\"idCountry\":null,\"team\":null,\"user"
                                        + "\":null}}"));
    }
}

