package com.dharbor.talent.managervacations.securityservice.service;


import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.securityservice.repository.UserRepository;
import com.dharbor.talent.managervacations.securityservice.validator.Verifications;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    private Verifications verifications;

    private Message message;

    private UserServiceImpl undertest;


    @BeforeEach
    void setUp(){
        verifications = new Verifications();
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        message = new Message(resourceBundleMessageSource);
        undertest = new UserServiceImpl(userRepository,verifications,message);
    }


    @Test
    void save(){
        User userToSave = new User();
        userToSave.setEmail("diana@gmail.com");
        userToSave.setPassword("1234");
        userToSave.setUserType(UserType.COLLABORATOR);
        userToSave.setCreatedDate(new Date());

        User userSaved = new User();
        userSaved.setEmail(userToSave.getEmail());
        userSaved.setPassword(userToSave.getPassword());
        userSaved.setUserType(userToSave.getUserType());
        userSaved.setCreatedDate(userToSave.getCreatedDate());
        userSaved.setId(1L);

        Mockito.when(userRepository.save(userToSave)).thenReturn(userSaved);

        User result = undertest.save(userToSave);

        verify(userRepository, times(1)).save(userToSave);
        assertTrue(result.getId().compareTo(1L) == 0);
    }

    @Test
    void saveWithEmailNull_ThrowsBadRequest(){
        User userToSave = new User();
        userToSave.setEmail(null);
        userToSave.setPassword("1234");
        userToSave.setUserType(UserType.COLLABORATOR);
        userToSave.setCreatedDate(new Date());

        Exception exception = assertThrows(BadRequestException.class, () ->{
            User result = undertest.save(userToSave);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "User email cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveWithPasswordNull_ThrowsBadRequest(){
        User userToSave = new User();
        userToSave.setEmail("diana@gmail.com");
        userToSave.setPassword(null);
        userToSave.setUserType(UserType.COLLABORATOR);
        userToSave.setCreatedDate(new Date());

        Exception exception = assertThrows(BadRequestException.class, () ->{
            User result = undertest.save(userToSave);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "User password cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveWithDateNull_ThrowsBadRequest(){
        User userToSave = new User();
        userToSave.setEmail("diana@gmail.com");
        userToSave.setPassword("123");
        userToSave.setUserType(UserType.COLLABORATOR);
        userToSave.setCreatedDate(null);

        Exception exception = assertThrows(BadRequestException.class, () ->{
            User result = undertest.save(userToSave);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "User Date cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveWithUserTypeNull_ThrowsBadRequest(){
        User userToSave = new User();
        userToSave.setEmail("diana@gmail.com");
        userToSave.setPassword("123");
        userToSave.setUserType(null);
        userToSave.setCreatedDate(new Date());

        Exception exception = assertThrows(BadRequestException.class, () ->{
            User result = undertest.save(userToSave);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "User Type not Found";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveDuplicate_ThrowsBadRequest(){
        User userToSave = new User();
        userToSave.setEmail("diana@gmail.com");
        userToSave.setPassword("123");
        userToSave.setUserType(UserType.COLLABORATOR);
        userToSave.setCreatedDate(new Date());

        User userSaved = new User();
        userSaved.setEmail(userToSave.getEmail());
        userSaved.setPassword(userToSave.getPassword());
        userSaved.setUserType(userToSave.getUserType());
        userSaved.setCreatedDate(userToSave.getCreatedDate());
        userSaved.setId(1L);

        Mockito.when(userRepository.existsByEmail(userSaved.getEmail())).thenReturn(true);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.save(userToSave);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "The email is already registered";

        assertTrue(actualMessage.contains(expectedMessage));
    }



    @Test
    void deleteUserByIdNotExists_throwsBadRequest() {
        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("1234");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());

        Mockito.when(userRepository.existsById(user.getId())).thenReturn(false);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.deleteUserById(user.getId());
        });

        String actualMessage = exception.getMessage();
        String expectedMessage = "User Not Found";

        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void deleteUserById() {
        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("1234");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());

        Mockito.when(userRepository.existsById(user.getId())).thenReturn(true);

        undertest.deleteUserById(user.getId());
        verify(userRepository).deleteById(user.getId());
    }


    @Test
    void existUserbyId() {

        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("1234");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());
        userRepository.save(user);

        Mockito.when(userRepository.existsById(user.getId())).thenReturn(true);
        Boolean result = undertest.existUserbyId(user.getId());

        verify(userRepository).existsById(user.getId());
        assertTrue(result);
    }

    @Test
    void doesNotexistUserbyId() {
        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("1234");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());

        Mockito.when(userRepository.existsById(user.getId())).thenReturn(false);
        Boolean result = undertest.existUserbyId(user.getId());

        verify(userRepository).existsById(user.getId());
        assertTrue(!result);
    }
    @Test
    void findById() {

        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("1234");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());
        userRepository.save(user);

        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Optional<User> result = undertest.findById(user.getId());

        verify(userRepository).findById(user.getId());
        Assertions.assertEquals(true, result.isPresent());
    }


    @Test
    void findByIdofNull() {
        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("1234");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());
        userRepository.save(user);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(null);
        assertThrows(NullPointerException.class, () ->{
             undertest.findById(user.getId());
        });

    }


}