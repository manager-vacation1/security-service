package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.securityservice.repository.ProfileRepository;
import com.dharbor.talent.managervacations.securityservice.repository.UserRepository;
import com.dharbor.talent.managervacations.securityservice.validator.Verifications;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class ProfileServiceImplTest {

    @Mock
    private ProfileRepository profileRepository;

    private Verifications verifications;

    private Message message;

    private ProfileServiceImpl undertest;

    private User user;
    private  Team team;
    private Profile profile;

    @BeforeEach
    void setUp(){
        verifications = new Verifications();
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        message = new Message(resourceBundleMessageSource);
        undertest = new ProfileServiceImpl(profileRepository,verifications,message);

        user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("12345");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());

        team = new Team();
        team.setTeamName("Team - DH");
        team.setId(1);

        profile = new Profile();
        profile.setFirstName("Diana");
        profile.setLastName("Vargas");
        profile.setNickName("DIANA");
        profile.setImageMongoId("img..");
        profile.setIdCountry(1L);
        profile.setBirthdate(new Date());
        Short shortNumber = 1;
        profile.setNumberOfOffsets(shortNumber);
        profile.setUser(user);
        profile.setTeam(team);
    }

    @Test
    void save() {
        Profile profileSaved = new Profile();
        profileSaved.setFirstName(profile.getFirstName());
        profileSaved.setLastName(profile.getLastName());
        profileSaved.setNickName(profile.getNickName());
        profileSaved.setImageMongoId(profile.getImageMongoId());
        profileSaved.setIdCountry(profile.getIdCountry());
        profileSaved.setBirthdate(profile.getBirthdate());
        profileSaved.setNumberOfOffsets(profile.getNumberOfOffsets());
        profileSaved.setUser(profile.getUser());
        profileSaved.setTeam(profile.getTeam());
        profile.setId(1L);

        Mockito.when(profileRepository.save(profile)).thenReturn(profileSaved);
        Profile result = undertest.save(profile);

        verify(profileRepository, times(1)).save(profile);
        assertTrue(result.getFirstName().equals(profileSaved.getFirstName()));
    }

    @Test
    void saveFirstNameNull_throwsBadRequest(){
        profile.setFirstName(null);
        Exception exception = assertThrows(BadRequestException.class, () ->{
           undertest.save(profile);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "Profile firstName cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveLastNameNull_throwsBadRequest(){
        profile.setLastName(null);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.save(profile);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "Profile lastName cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveNicknameNull_throwsBadRequest(){
        profile.setNickName(null);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.save(profile);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "Profile nickName cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveBirthdateNull_throwsBadRequest(){
        profile.setBirthdate(null);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.save(profile);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "Profile BirthDay cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveTeamNull_throwsBadRequest(){
        team.setId(null);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.save(profile);
        });
        String actualMessage = exception.getMessage();
        String expectedMessage = "Profile idteam cannot be null or less than 0";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void findByUser_Id() {

        user.setId(1L);
        Mockito.when(profileRepository.existsByUser_Id(user.getId())).thenReturn(true);
        Mockito.when(profileRepository.findByUser_Id(user.getId())).thenReturn(profile);
        Profile result = undertest.findByUser_Id(user.getId());

        assertTrue(result != null);
    }

    @Test
    void findByUser_IdNull() {

        user.setId(1L);
        Mockito.when(profileRepository.findByUser_Id(user.getId())).thenReturn(profile);

        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.findByUser_Id(user.getId());
        });

        String actualMessage = exception.getMessage();
        String expectedMessage = "User Not Found";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void existsByUser_Id() {

        Mockito.when(profileRepository.existsByUser_Id(profile.getId())).thenReturn(true);
        Boolean result = undertest.existsByUser_Id(profile.getId());

        verify(profileRepository).existsByUser_Id(profile.getId());
        assertTrue(result);

    }

    @Test
    void findAllByTeam() {
        List<Profile> profiles =new ArrayList<>();
        profiles.add(profile);
        Mockito.when(profileRepository.findAllByTeam(team)).thenReturn(profiles);

        List<Profile> result = undertest.findAllByTeam(team);
        verify(profileRepository).findAllByTeam(profile.getTeam());
        assertTrue(result.contains(profile));
    }

    @Test
    void existsByTeamAndUser_UserType() {
        Mockito.when(profileRepository.existsByTeamAndUser_UserType(team, UserType.COLLABORATOR)).thenReturn(true);
        Boolean result = undertest.existsByTeamAndUser_UserType(team, UserType.COLLABORATOR);

        verify(profileRepository).existsByTeamAndUser_UserType(profile.getTeam(), profile.getUser().getUserType());
        assertTrue(result);
    }

    @Test
    void deleteByUser_Id() {
        Mockito.doNothing().when(profileRepository).deleteByUser_Id(profile.getId());
        verify(profileRepository, times(0)).deleteByUser_Id(profile.getId());
    }

    @Test
    void findByTeamAndUser_UserType() {
        Mockito.when(profileRepository.findByTeamAndUser_UserType(team, UserType.COLLABORATOR)).thenReturn(profile);
        Profile result = undertest.findByTeamAndUser_UserType(team, UserType.COLLABORATOR);

        verify(profileRepository).findByTeamAndUser_UserType(team, UserType.COLLABORATOR);
        assertTrue(result != null);
    }
}