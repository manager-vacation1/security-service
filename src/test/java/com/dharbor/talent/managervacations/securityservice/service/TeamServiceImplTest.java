package com.dharbor.talent.managervacations.securityservice.service;

import com.dharbor.talent.managervacations.securityservice.common.Message;
import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import com.dharbor.talent.managervacations.securityservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.securityservice.repository.TeamRepository;
import com.dharbor.talent.managervacations.securityservice.repository.UserRepository;
import com.dharbor.talent.managervacations.securityservice.validator.Verifications;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class TeamServiceImplTest {

    @Mock
    private TeamRepository teamRepository;

    private Verifications verifications;

    private Message message;

    private TeamServiceImpl undertest;


    @BeforeEach
    void setUp(){
        verifications = new Verifications();
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        message = new Message(resourceBundleMessageSource);
        undertest = new TeamServiceImpl(teamRepository,verifications,message);
    }

    @Test
    void save() {
        Team teamToSave = new Team();
        teamToSave.setTeamName("team - DH");

        Team teamSaved = new Team();
        teamSaved.setTeamName(teamToSave.getTeamName());
        teamSaved.setId(1);

        Mockito.when(teamRepository.save(teamToSave)).thenReturn(teamSaved);

        Team result = undertest.save(teamToSave);

        verify(teamRepository, times(1)).save(teamToSave);
        assertTrue(result.getId() == 1);
    }

    @Test
    void saveDuplicate() {
        Team teamToSave = new Team();
        teamToSave.setTeamName("team - DH");

        Team teamSaved = new Team();
        teamSaved.setTeamName(teamToSave.getTeamName());
        teamSaved.setId(1);

        Mockito.when(teamRepository.existsByTeamName(teamToSave.getTeamName())).thenReturn(true);
        Exception exception = assertThrows(BadRequestException.class, () ->{
            undertest.save(teamToSave);
        });

        String actualMessage = exception.getMessage();
        String expectedMessage = "That team name already exists";

        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void findById() {

        Team team = new Team();
        team.setTeamName("team - DH");
        team.setId(1);
        teamRepository.save(team);

        Mockito.when(teamRepository.findById(team.getId())).thenReturn(Optional.of(team));
        Optional<Team> result = undertest.findById(team.getId());

        verify(teamRepository).findById(team.getId());
        Assertions.assertEquals(true, result.isPresent());
    }

    @Test
    void findByIdofNull() {

        Team team = new Team();
        team.setTeamName("team - DH");

        Mockito.when(teamRepository.findById(team.getId())).thenReturn(Optional.of(team));
        Exception exception = assertThrows(BadRequestException.class, () ->{
             undertest.findById(team.getId());
        });

        String actualMessage = exception.getMessage();
        String expectedMessage = "Team Name cannot be null or empty";

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void findAll() {

        Team team = new Team();
        team.setTeamName("team - DH");

        List<Team> teams = new ArrayList<>();
        teams.add(team);
        Mockito.when(teamRepository.findAll()).thenReturn(teams);

        List<Team> result = undertest.findAll();
        verify(teamRepository).findAll();
        Assertions.assertTrue(result.contains(team));
    }
}