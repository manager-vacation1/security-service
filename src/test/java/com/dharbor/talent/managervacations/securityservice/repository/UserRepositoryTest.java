package com.dharbor.talent.managervacations.securityservice.repository;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private UserRepository undertest;

    @Test
    void existsByEmail() {
        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("12345");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());
        undertest.save(user);

        Boolean result = undertest.existsByEmail(user.getEmail());

        assertThat(result).isTrue();
    }

    @Test
    void doesNotExistsByEmail() {

        Boolean result = undertest.existsByEmail("adrian@gmail.com");
        assertThat(result).isFalse();
    }

    @Test
    void existsById() {

        User user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("12345");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());
        undertest.save(user);

        Boolean result = undertest.existsById(user.getId());

        assertThat(result).isTrue();
    }

    @Test
    void existsByIdNull() {

        Boolean result = undertest.existsById(100L);
        assertThat(result).isFalse();
    }
}