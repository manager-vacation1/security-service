package com.dharbor.talent.managervacations.securityservice.repository;

import com.dharbor.talent.managervacations.securityservice.domain.Team;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class TeamRepositoryTest {

    @Autowired
    private TeamRepository undertest;

    @Test
    void existsByTeamName() {
        Team teamTest = new Team();
        teamTest.setTeamName("Team - DH");
        undertest.save(teamTest);

        Boolean result = undertest.existsByTeamName(teamTest.getTeamName());
        assertThat(result).isTrue();
    }
    @Test
    void doesNotExistsByTeamName() {

        Boolean result = undertest.existsByTeamName("Team - F");
        assertThat(result).isFalse();
    }
}