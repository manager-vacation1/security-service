package com.dharbor.talent.managervacations.securityservice.repository;

import com.dharbor.talent.managervacations.securityservice.constant.UserType;
import com.dharbor.talent.managervacations.securityservice.domain.Profile;
import com.dharbor.talent.managervacations.securityservice.domain.Team;
import com.dharbor.talent.managervacations.securityservice.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.swing.text.StyledEditorKit;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class ProfileRepositoryTest {

    @Autowired
    private ProfileRepository undertest;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamRepository teamRepository;

    private User user;
    private Team team;
    private Profile profile;


    @BeforeEach
    void setUp() {
        user = new User();
        user.setEmail("diana@gmail.com");
        user.setPassword("12345");
        user.setUserType(UserType.COLLABORATOR);
        user.setCreatedDate(new Date());
        userRepository.save(user);

        team = new Team();
        team.setTeamName("Team - DH");
        teamRepository.save(team);

        profile = new Profile();
        profile.setFirstName("Diana");
        profile.setLastName("Vargas");
        profile.setNickName("DIANA");
        profile.setImageMongoId("img..");
        profile.setIdCountry(1L);
        profile.setBirthdate(new Date());
        Short shortNumber = 1;
        profile.setNumberOfOffsets(shortNumber);
        profile.setUser(user);
        profile.setTeam(team);
        undertest.save(profile);
    }



    @Test
    void findByUser_IdNull() {
        Profile result = undertest.findByUser_Id(5L);
        assertThat(result).isNull();
    }

    @Test
    void findByUser_IdNotNull() {
        Profile result = undertest.findByUser_Id(user.getId());
        assertThat(result).isNotNull();
    }

    @Test
    void findByUser_Id() {
        Profile result = undertest.findByUser_Id(user.getId());
        assertThat(result.equals(profile)).isTrue();
    }

    @Test
    void existsByUser_Id() {
        Boolean result = undertest.existsById(user.getId());
        assertThat(result).isTrue();
    }

    @Test
    void doesNotExistsByUser_Id() {
        Boolean result = undertest.existsById(8L);
        assertThat(result).isFalse();
    }


    @Test
    void findAllByTeamNull() {
        Team teamQA = new Team();
        teamQA.setTeamName("Team - QA");
        teamRepository.save(teamQA);

        List<Profile> result = undertest.findAllByTeam(teamQA);
        assertThat(result.isEmpty()).isTrue();
    }

    @Test
    void findAllByTeamNotNull() {
        List<Profile> result = undertest.findAllByTeam(team);
        assertThat(result).isNotNull();
    }

    @Test
    void findAllByTeam() {
        List<Profile> result = undertest.findAllByTeam(team);
        assertThat(result.contains(profile)).isTrue();
    }

    @Test
    void existsByTeamAndUser_UserType() {
        Boolean result = undertest.existsByTeamAndUser_UserType(team, user.getUserType());
        assertThat(result).isTrue();
    }

    @Test
    void doesNotExistsByTeamAndUser_UserType() {
        Boolean result = undertest.existsByTeamAndUser_UserType(team, UserType.MANAGER);
        assertThat(result).isFalse();
    }


    @Test
    void deleteByUser_Id() {
        undertest.deleteByUser_Id(user.getId());
        Profile result = undertest.findByUser_Id(user.getId());
        assertThat(result).isNull();
    }

    @Test
    void deleteByUser_IdNotNull() {
        undertest.deleteByUser_Id(4L);
        Profile result = undertest.findByUser_Id(user.getId());
        assertThat(result).isNotNull();
    }

    @Test
    void findByTeamAndUser_UserTypeNull() {
        Profile result = undertest.findByTeamAndUser_UserType(team, UserType.MANAGER);
        assertThat(result).isNull();
    }

    @Test
    void findByTeamAndUser_UserTypeNotNull() {
        Profile result = undertest.findByTeamAndUser_UserType(team, user.getUserType());
        assertThat(result).isNotNull();
    }

    @Test
    void findByTeamAndUser_UserType() {
        Profile result = undertest.findByTeamAndUser_UserType(team, user.getUserType());
        assertThat(result.equals(profile)).isTrue();
    }
}